# Test Technique Java Backend - Ekwateur

Java: version 11

Spring Boot: version 2.7

Base de données: In-memory H2

Tests unitaires: JUnit

## Comment tester l'application
Il suffit de démarrer le projet puis le tester dans Postman. Les données 
sont insérées automatiquement via le 
fichier "**src/main/resources/script.sql**".

- Pour calculer la consommation d'énergie pour un particulier:  
POST http://localhost:8080/api/{{customerId}}/energy-consumption


- Pour calculer la consommation d'énergie pour un pro:  
POST http://localhost:8080/api/pro/{{customerId}}/energy-consumption

## Captures d'écran
Consommation pour un particulier
![Consommation pour un particulier](https://i.ibb.co/2vY7cy5/image.png)


Client introuvable
![Client introuvable](https://i.ibb.co/9V1M7DY/image.png)