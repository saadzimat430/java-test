DROP TABLE IF EXISTS client_pro;

DROP TABLE IF EXISTS client_particulier;

CREATE TABLE client_pro
(
    id               INT PRIMARY KEY,
    reference        VARCHAR(11) UNIQUE,
    siret            BIGINT,
    raison_sociale   VARCHAR(255),
    chiffre_affaires DECIMAL(19, 2),
    CONSTRAINT chk_reference_regex_pro CHECK (reference ~ '^EKW\d{8}$')
);

CREATE TABLE client_particulier
(
    id        INT PRIMARY KEY,
    reference VARCHAR(11) UNIQUE,
    civilite  VARCHAR(255),
    nom       VARCHAR(255),
    prenom    VARCHAR(255),
    CONSTRAINT chk_reference_regex CHECK (reference ~ '^EKW\d{8}$')
);

-- Inserting data into client_pro table
INSERT INTO client_pro (id, reference, siret, raison_sociale, chiffre_affaires)
VALUES (1, 'EKW00000001', '12345678900001', 'Ekwateur', 900000.00);

INSERT INTO client_pro (id, reference, siret, raison_sociale, chiffre_affaires)
VALUES (2, 'EKW00000005', '98765432100002', 'EDF', 5040000.87);

-- Inserting data into client_particulier table
INSERT INTO client_particulier (id, reference, civilite, nom, prenom)
VALUES (1, 'EKW00000013', 'Monsieur', 'Doe', 'John');

INSERT INTO client_particulier (id, reference, civilite, nom, prenom)
VALUES (2, 'EKW00000019', 'Madame', 'Smith', 'Jane');
