package com.ekwateur.test;

import com.ekwateur.test.dao.exception.ClientNotFoundException;
import com.ekwateur.test.dao.repository.ClientProRepository;
import com.ekwateur.test.dao.service.EnergyService;
import com.ekwateur.test.dto.EnergyConsumption;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
@ActiveProfiles("test")
class EkwateurJavaTestApplicationTests {

    private final EnergyService energyService;
    private final ClientProRepository clientProRepository;

    @Autowired
    public EkwateurJavaTestApplicationTests(EnergyService energyService, ClientProRepository clientProRepository) {
        this.energyService = energyService;
        this.clientProRepository = clientProRepository;
    }

    @Nested
    @DisplayName("Unit tests for energy bill computation")
    @Sql("classpath:script-test.sql")
    class EnergyTests {
        @Test
        void should_calculate_energy_consumption_for_individual_customer() throws ClientNotFoundException {
            Double energyConso = energyService.calculateEnergyConsumption(1L,
                    EnergyConsumption.builder().consoElectricite(6.00).consoGaz(2.33).build(),
                    false);
            assertEquals(0.99395, energyConso);
        }

        @Test
        void should_calculate_energy_consumption_for_pro_customer_if_ca_less_than_million() throws ClientNotFoundException {
            Double energyConso = energyService.calculateEnergyConsumption(1L,
                    EnergyConsumption.builder().consoElectricite(7.00).consoGaz(3.00).build(),
                    true);
            assertEquals(1.165, energyConso);
        }

        @Test
        void should_calculate_energy_consumption_for_pro_customer_if_ca_more_than_million() throws ClientNotFoundException {
            Double energyConso = energyService.calculateEnergyConsumption(2L,
                    EnergyConsumption.builder().consoElectricite(6.00).consoGaz(2.00).build(),
                    true);
            assertEquals(0.906, energyConso);
        }

        @Test
        void should_throw_exception_for_invalid_customer() {
            assertThrows(ClientNotFoundException.class, () -> energyService.calculateEnergyConsumption(
                    99L, EnergyConsumption.builder().consoElectricite(9.00).consoGaz(1.00).build(), true));
        }
    }

}
