package com.ekwateur.test.controller;

import com.ekwateur.test.dao.exception.ClientNotFoundException;
import com.ekwateur.test.dao.service.EnergyService;
import com.ekwateur.test.dto.EnergyConsumption;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api")
public class EnergyController {
    private final EnergyService energyService;

    public EnergyController(EnergyService energyService) {
        this.energyService = energyService;
    }

    @PostMapping(path = "/{customerId}/energy-consumption")
    public ResponseEntity<Map<String, Double>> getEnergyConsumption(@PathVariable Long customerId,
                                                                    @RequestBody EnergyConsumption energyConsumption) throws ClientNotFoundException {
        Map<String, Double> data = new HashMap<>();
        if (energyConsumption != null) {
            Double amount = energyService.calculateEnergyConsumption(customerId, energyConsumption, false);
            data.put("amount", amount);
        }
        return ResponseEntity.ok(data);
    }

    @PostMapping(path = "/pro/{customerId}/energy-consumption")
    public ResponseEntity<Map<String, Double>> getEnergyConsumptionPro(@PathVariable Long customerId,
                                                                    @RequestBody EnergyConsumption energyConsumption) throws ClientNotFoundException {
        Map<String, Double> data = new HashMap<>();
        if (energyConsumption != null) {
            Double amount = energyService.calculateEnergyConsumption(customerId, energyConsumption, true);
            data.put("amount", amount);
        }
        return ResponseEntity.ok(data);
    }

}
