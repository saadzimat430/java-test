package com.ekwateur.test.dao.service;

import com.ekwateur.test.dao.entity.ClientParticulier;
import com.ekwateur.test.dao.entity.ClientPro;
import com.ekwateur.test.dao.exception.ClientNotFoundException;
import com.ekwateur.test.dao.repository.ClientParticulierRepository;
import com.ekwateur.test.dao.repository.ClientProRepository;
import com.ekwateur.test.dto.EnergyConsumption;
import org.springframework.stereotype.Service;

@Service
public class EnergyService {
    private final ClientProRepository clientProRepository;
    private final ClientParticulierRepository clientParticulierRepository;

    public EnergyService(ClientProRepository clientProRepository, ClientParticulierRepository clientParticulierRepository) {
        this.clientProRepository = clientProRepository;
        this.clientParticulierRepository = clientParticulierRepository;
    }

    public Double calculateEnergyConsumption(Long customerId, EnergyConsumption energyConsumption, Boolean isPro) throws ClientNotFoundException {
        Double amount;
        // si le client est professionnel
        if (Boolean.TRUE.equals(isPro)) {
            ClientPro client = clientProRepository.findById(customerId).orElseThrow(() -> new ClientNotFoundException("Client does not exist"));
            if (client.getChiffreAffaires() > 1000000L)
                amount = (0.114 * energyConsumption.getConsoElectricite()) + (0.111 * energyConsumption.getConsoGaz());
            else
                amount = (0.118 * energyConsumption.getConsoElectricite()) + (0.113 * energyConsumption.getConsoGaz());
        } else {
            // si le client est particulier
            ClientParticulier client = clientParticulierRepository.findById(customerId).orElseThrow(() -> new ClientNotFoundException("Client does not exist"));
            amount = (0.121 * energyConsumption.getConsoElectricite()) + (0.115 * energyConsumption.getConsoGaz());
        }
        return amount;
    }
}
