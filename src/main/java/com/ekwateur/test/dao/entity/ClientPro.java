package com.ekwateur.test.dao.entity;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;

@Getter
@Entity
@NoArgsConstructor
public class ClientPro extends Client {

    @Column(nullable = false, precision = 14)
    private Long siret;

    @Column(nullable = false)
    private String raisonSociale;

    @Column(nullable = false, scale = 2)
    private Double chiffreAffaires;

    @Builder
    public ClientPro(String reference, Long siret, String raisonSociale, Double chiffreAffaires) {
        super(reference);
        this.siret = siret;
        this.raisonSociale = raisonSociale;
        this.chiffreAffaires = chiffreAffaires;
    }

}
