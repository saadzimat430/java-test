package com.ekwateur.test.dao.entity;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class ClientParticulier extends Client {

    @Column
    private String civilite;

    @Column(nullable = false)
    private String nom;

    @Column(nullable = false)
    private String prenom;

}
