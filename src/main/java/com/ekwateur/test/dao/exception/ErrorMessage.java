package com.ekwateur.test.dao.exception;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ErrorMessage {

    private String message;
}
