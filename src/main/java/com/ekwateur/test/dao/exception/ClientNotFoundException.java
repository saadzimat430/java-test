package com.ekwateur.test.dao.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class ClientNotFoundException extends Exception {

    private String message;
}
