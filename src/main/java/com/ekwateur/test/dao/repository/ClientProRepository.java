package com.ekwateur.test.dao.repository;

import com.ekwateur.test.dao.entity.ClientPro;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClientProRepository extends JpaRepository<ClientPro, Long> {
}
