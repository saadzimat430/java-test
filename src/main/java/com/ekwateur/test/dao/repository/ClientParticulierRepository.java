package com.ekwateur.test.dao.repository;

import com.ekwateur.test.dao.entity.ClientParticulier;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClientParticulierRepository extends JpaRepository<ClientParticulier, Long> {
}
