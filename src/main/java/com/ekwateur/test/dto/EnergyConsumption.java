package com.ekwateur.test.dto;


import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class EnergyConsumption {

    private Double consoElectricite;

    private Double consoGaz;

    public EnergyConsumption(Double consoElectricite, Double consoGaz) {
        this.consoElectricite = (consoElectricite != null) ? consoElectricite : 0.00;
        this.consoGaz = (consoGaz != null) ? consoGaz : 0.00;
    }
}
