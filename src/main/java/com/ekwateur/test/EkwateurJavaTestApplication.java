package com.ekwateur.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EkwateurJavaTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(EkwateurJavaTestApplication.class, args);
    }

}
